console.log(' Hello World ');

/*objective 1*/


let firstName = 'Miguel';
	console.log('First Name: ' + firstName);

let lastName = 'Yulo';
	console.log('Last Name: ' + lastName);

let age = 25;
	console.log('Age: ' + age);

let hobbies = ['Basketball','Movies','Flying'];
	console.log('Hobbies: ')
	console.log(hobbies);

let address = {
		houseNumber: '53',
		street: 'Luna',
		city: 'Antipolo',
		state: 'Rizal',
	}

	console.log('Work Address: ');
	console.log(address);


/*objective 2*/
	let fullName = 'Steve Rogers';
	console.log('My full name is: ' + fullName);

	let currentAge = 40;
	console.log('My current age is: ' + currentAge);
	
	let friends = ['Tony','Bruce','Thor','Natasha','Clint','Nick'];
	console.log('My Friends are: ');
	console.log(friends);

	let profile = {
		userName: 'captain_america',
		fullName: 'Steve Rogers',
		age: 40,
		isActive: false,
	}

	console.log('My Full Profile: ');
	console.log(profile);

	let friendFullName = 'Bucky Barnes';
	console.log('My bestfriend is: ' + friendFullName);

	const lastLocation = 'Arctic Ocean';
	console.log('I was found frozen in: ' + lastLocation);

